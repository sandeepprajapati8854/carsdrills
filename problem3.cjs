function problem3(inventory) {
  let allCarModel = [];

  if (inventory.length == 0) {
    return inventory;
  }
  if (Array.isArray(inventory) == false) {
    return [];
  }

  for (let index = 0; index < inventory.length; index++) {
    allCarModel.push(inventory[index].car_model);
  }
  return allCarModel.sort();

  // for (let index = 0; index < inventory.length; index++) {
  //   allCarModel.push(inventory[index].car_model);
  // }
  // let upperCaseArray = [];
  // for (let index = 0; index < allCarModel.length; index++) {
  //   let carModelName = allCarModel[index];
  //   let upper = carModelName.toUpperCase();
  //   upperCaseArray.push(upper);
  // }
  // return upperCaseArray.sort();
}

module.exports = problem3;
