function problem2(inventory) {
  if (inventory.length == 0) {
    return inventory;
  }
  if (Array.isArray(inventory) == false) {
    return [];
  }
  return inventory[inventory.length - 1];

  // let returnResult;
  // if (inventory.length == 0) {
  //   returnResult = "No data found!";
  //   return returnResult;
  // }
  // returnResult = `Last car id is ${
  //   inventory[inventory.length - 1].id
  // } and car make ${inventory[inventory.length - 1].car_make} and car model ${
  //   inventory[inventory.length - 1].car_model
  // }`;
  // return returnResult;
}

module.exports = problem2;
