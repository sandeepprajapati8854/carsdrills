const problem1 = require("../problem1.cjs");
let inventory = require("../dataCars.cjs");

let resultObj = problem1(inventory, 33);

let result = Array.from(Object.values(resultObj));
console.log(result);
console.log(
  `Car ${result[0]} is a ${result[result.length - 1]} ${result[1]} ${result[2]}`
);
