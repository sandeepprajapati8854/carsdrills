function problem1(inventory, carId) {
  if (Array.isArray(inventory) == false) {
    return [];
  }
  if (inventory.length == 0) {
    return inventory;
  }
  if (carId == undefined) {
    return [];
  }
  if (Number.isNaN(carId) == true) {
    return [];
  }
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id == carId) {
      return inventory[index];
    }
    if (index == inventory.length - 1) {
      return [];
    }
  }
}
module.exports = problem1;
