function problem6(inventory) {
  let allAudiAndBmw = [];
  if (inventory.length == 0) {
    return inventory;
  }
  if (Array.isArray(inventory) == false) {
    return [];
  }
  for (let index = 0; index < inventory.length; index++) {
    if (
      inventory[index].car_make === "Audi" ||
      inventory[index].car_make === "BMW"
    ) {
      allAudiAndBmw.push(inventory[index]);
    }
  }
  return allAudiAndBmw;
}

module.exports = problem6;
