function problem4(inventory) {
  let allCarYear = [];
  if (inventory.length == 0) {
    return inventory;
  }
  if (Array.isArray(inventory) == false) {
    return [];
  }
  for (let index = 0; index < inventory.length; index++) {
    allCarYear.push(inventory[index].car_year);
  }
  return allCarYear;
}

module.exports = problem4;
