function problem5(inventory) {
  let oldCar = [];
  if (inventory.length == 0) {
    return inventory;
  }
  if (Array.isArray(inventory) == false) {
    return [];
  }
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].car_year < 2000) {
      oldCar.push(inventory[index].car_make);
    }
  }
  return oldCar;
}

module.exports = problem5;
